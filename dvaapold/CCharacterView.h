//
//  CCharacterView.h
//  dvaapold
//
//  Created by Milan Kazarka on 1/21/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import "CSpriteView.h"
#import "CSpriteImage.h"

enum _CHARACTER_ORIENTATIONS {
    _CHARACTER_ORIENTATION_LEFT,
    _CHARACTER_ORIENTATION_RIGHT,
    _CHARACTER_ORIENTATION_UP,
    _CHARACTER_ORIENTATION_DOWN,
    _CHARACTER_ORIENTATION_UPLEFT,
    _CHARACTER_ORIENTATION_UPRIGHT,
    _CHARACTER_ORIENTATION_DOWNLEFT,
    _CHARACTER_ORIENTATION_DOWNRIGHT
};

@interface CCharacterView : CSpriteView {
@public
}

@property (nonatomic) int orientation;
@property (nonatomic) BOOL moving; // if the character is moving we toggle leg movement (left/right)
@property NSMutableDictionary *resources;

-(int)loadCharacterResourcePackNamed:(NSString*)name;

@end
