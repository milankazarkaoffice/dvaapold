//
//  CCharacterView.m
//  dvaapold
//
//  Created by Milan Kazarka on 1/21/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import "CCharacterView.h"

@implementation CCharacterView

@synthesize moving = _moving;

-(id)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self)
    {
        self.orientation = _CHARACTER_ORIENTATION_RIGHT;
        self.moving = NO;
        self.resources = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)setOrientation:(int)orientation
{
    _orientation = orientation;
    
    CSpriteImage *csi = [self.resources objectForKey:[NSNumber numberWithInt:orientation]];
    if (csi)
    {
        [self.imageView setImage:csi.image];
    }
}

-(int)loadCharacterResourcePackNamed:(NSString*)name
{
    if (!name)
        return 1;
    
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Left.png",name]];
    if (image)
    {
        CSpriteImage *csi = [[CSpriteImage alloc] initWithImage:image];
        [self.resources setObject:csi forKey:[NSNumber numberWithInt:_CHARACTER_ORIENTATION_LEFT]];
    }
        
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Right.png",name]];
    if (image)
    {
        CSpriteImage *csi = [[CSpriteImage alloc] initWithImage:image];
        [self.resources setObject:csi forKey:[NSNumber numberWithInt:_CHARACTER_ORIENTATION_RIGHT]];
    }
        
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Up.png",name]];
    if (image)
    {
        CSpriteImage *csi = [[CSpriteImage alloc] initWithImage:image];
        [self.resources setObject:csi forKey:[NSNumber numberWithInt:_CHARACTER_ORIENTATION_UP]];
    }
        
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Down.png",name]];
    if (image)
    {
        CSpriteImage *csi = [[CSpriteImage alloc] initWithImage:image];
        [self.resources setObject:csi forKey:[NSNumber numberWithInt:_CHARACTER_ORIENTATION_DOWN]];
    }
    
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@UpRight.png",name]];
    if (image)
    {
        CSpriteImage *csi = [[CSpriteImage alloc] initWithImage:image];
        [self.resources setObject:csi forKey:[NSNumber numberWithInt:_CHARACTER_ORIENTATION_UPRIGHT]];
    }
    
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@DownRight.png",name]];
    if (image)
    {
        CSpriteImage *csi = [[CSpriteImage alloc] initWithImage:image];
        [self.resources setObject:csi forKey:[NSNumber numberWithInt:_CHARACTER_ORIENTATION_DOWNRIGHT]];
    }
    
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@UpLeft.png",name]];
    if (image)
    {
        CSpriteImage *csi = [[CSpriteImage alloc] initWithImage:image];
        [self.resources setObject:csi forKey:[NSNumber numberWithInt:_CHARACTER_ORIENTATION_UPLEFT]];
    }
    
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@DownLeft.png",name]];
    if (image)
    {
        CSpriteImage *csi = [[CSpriteImage alloc] initWithImage:image];
        [self.resources setObject:csi forKey:[NSNumber numberWithInt:_CHARACTER_ORIENTATION_DOWNLEFT]];
    }
    
    return 0;
}

-(void)setMoving:(BOOL)moving
{
    @synchronized(self)
    {
        if (_moving==NO && moving==YES)
        {
            NSLog(@"    character moves");
        }
        else if (_moving==YES && moving==NO)
        {
            NSLog(@"    character stops");
        }
        
        _moving = moving;
    }
}

@end
