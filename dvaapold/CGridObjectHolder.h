//
//  CGridObjectHolder.h
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRpg.h"
#import "CSpriteView.h"

@interface CGridObjectHolder : UIView {
@public
}

@property CGridObject *go;
@property NSMutableArray *associatedSpriteViews; // sprite views associated with the grid object

-(CGPoint)getSpriteCoordinates;
-(CGPoint)getUIPositionForCenteredSprite:(CSpriteView*)sv;

-(void)associateSpriteView:(CSpriteView*)sv;

-(BOOL)isSpriteViewOnTop:(CSpriteView*)sv; // is sprite view on top of this grid view?
-(BOOL)isPosition:(CGPoint)pt; // is this position inside holder?

@end
