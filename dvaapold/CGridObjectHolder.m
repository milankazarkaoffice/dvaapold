//
//  CGridObjectHolder.m
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import "CGridObjectHolder.h"

@implementation CGridObjectHolder

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setBackgroundColor:[UIColor colorWithWhite:0.9f alpha:1.0f]];
    }
    return self;
}

-(CGPoint)getSpriteCoordinates
{
    CGPoint pt = CGPointZero;
    
    pt.x = self.frame.origin.x+(self.frame.size.width/2.0f);
    pt.y = self.frame.origin.y+(self.frame.size.height/2.0f);
    
    return pt;
}

-(void)genericInit
{
    self.associatedSpriteViews = [[NSMutableArray alloc] init];
}

-(CGPoint)getUIPositionForCenteredSprite:(CSpriteView*)sv
{
    CGPoint pt = CGPointZero;
    
    CGPoint centre = [self getSpriteCoordinates];
    pt.x = centre.x-(sv.frame.size.width/2.0f);
    pt.y = centre.y-(sv.frame.size.height/2.0f);
    
    return pt;
}

-(void)associateSpriteView:(CSpriteView*)sv
{
    if (!sv)
        return;
    [self.associatedSpriteViews addObject:sv];
}

-(BOOL)isSpriteViewOnTop:(CSpriteView*)sv
{
    if (!sv)
        return NO;
    
    CGPoint spt = [sv getPosition];
    if ([self isPosition:spt])
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)isPosition:(CGPoint)pt
{
    if (pt.x>self.frame.origin.x && pt.x<self.frame.origin.x+self.frame.size.width &&
        pt.y>self.frame.origin.y && pt.y<self.frame.origin.y+self.frame.size.height)
    {
        return YES;
    }
    
    return NO;
}

@end
