//
//  CPlainViewController.h
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCharacterView.h"
#import "CRpg.h"
#import "CGridObjectHolder.h"
#import "CTouchEventView.h"

@interface CPlainViewController : UIViewController <CTouchEventViewDelegate> {
@public
}

@property UIView *plainView;
@property CTouchEventView *touchViewOverlay;
@property NSMutableArray *grid; // array of (CGridObjectHolder) objects
@property NSMutableArray *sprites; // array of (CSpriteView) objects

@property CCharacterView *sv1;

-(CGridObjectHolder*)getGridObjectHolderAtPosition:(int)x y:(int)y;
-(void)addSpriteView:(CSpriteView*)sv onGridObject:(CGridObjectHolder*)gh;

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

@end
