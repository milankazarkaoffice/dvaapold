//
//  CPlainViewController.m
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import "CPlainViewController.h"

@interface CPlainViewController ()

@end

@implementation CPlainViewController

- (void)viewDidLoad
{
    self.sprites = [[NSMutableArray alloc] init];
    
    [super viewDidLoad];
    
    self.plainView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.plainView];
    
    self.touchViewOverlay = [[CTouchEventView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.touchViewOverlay];
    self.touchViewOverlay.delegate = self;
    
    [self initGrid];
    [self runTest];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self zsamplingThread];
    });
}

-(void)initGrid
{
    self.grid = [[NSMutableArray alloc] init];
    
    int gw = 21;
    int gh = 21;
    
    int x, y;
    for(y = 0; y < gh; y++)
    {
        for(x = 0; x < gw; x++)
        {
            CGridObject *go = new CGridObject(_CFRAMECREATE(x*(_GENERIC_SQ*4), y*(_GENERIC_SQ*2), _GENERIC_SQ*4, _GENERIC_SQ*2), x, y);
            go->tag = (gw*y)+x;
            CGridObjectHolder *holder = [[CGridObjectHolder alloc] initWithFrame:CGRectMake(go->mframe.origin.x,go->mframe.origin.y,go->mframe.size.width,go->mframe.size.height)];
            holder.go = go;
            [self.grid addObject:holder];
            
            if (go->tag % 2)
            {
                [holder setBackgroundColor:[UIColor whiteColor]];
            }
            [self.plainView addSubview:holder];
            
            CSpriteView *tile = [[CSpriteView alloc] initWithSize:CGSizeMake(_GENERIC_SQ*4,_GENERIC_SQ*2)];
            [tile setImage:[UIImage imageNamed:@"ground.png"]];
            [self addSpriteView:tile onGridObject:holder];
            tile.layer.zPosition = 0;
        }
    }
}

-(void)addSpriteView:(CSpriteView*)sv onGridObject:(CGridObjectHolder*)gh
{
    if (!sv || !gh)
        return;
    
    CGPoint pt = [gh getUIPositionForCenteredSprite:sv];
    [sv setFrame:CGRectMake(pt.x,pt.y,sv.frame.size.width,sv.frame.size.height)];
    
    [self.sprites addObject:sv];
    sv.layer.zPosition = ((gh.go->gy+1)*1000000)+([sv getPosition].y-gh.frame.origin.y);
    [self.plainView addSubview:sv];
}

-(CGridObjectHolder*)getGridObjectHolderAtPosition:(int)x y:(int)y
{
    CGridObjectHolder *holder = nil;
    
    int n;
    for(n = 0; n < [self.grid count]; n++)
    {
        holder = [self.grid objectAtIndex:n];
        if (holder)
        {
            if (holder.go)
            {
                if (holder.go->gx==x && holder.go->gy==y)
                    return holder;
            }
        }
    }
    return nil;
}

-(CGridObjectHolder*)getGridObjectHolderAtCoordinates:(CGPoint)pt
{
    CGridObjectHolder *holder = nil;
    
    int n;
    for(n = 0; n < [self.grid count]; n++)
    {
        holder = [self.grid objectAtIndex:n];
        if (holder)
        {
            if ([holder isPosition:pt])
                return holder;
        }
    }
    return nil;
}

-(void)zsamplingThread // mika - won't be needed once we do a propper route calculation system
{
    //return;
    while(1)
    {
        usleep(50000); // 20 fps
        dispatch_sync(dispatch_get_main_queue(), ^{
            int n;
            for(n = 0; n < [self.sprites count]; n++)
            {
                CSpriteView *sv = [self.sprites objectAtIndex:n];
                if (sv.layer.zPosition==0)
                    continue;
                if (sv)
                {
                    int nn;
                    CGridObjectHolder *gh;
                    for(nn = 0; nn < [self.grid count];nn++)
                    {
                        gh = [self.grid objectAtIndex:nn];
                        if ([gh isSpriteViewOnTop:sv])
                        {
                            sv.layer.zPosition = ((gh.go->gy+1)*1000000)+([sv getPosition].y-gh.frame.origin.y);
                            //NSLog(@"    zPosition(%f) tag(%d)",sv.layer.zPosition,sv.tag);
                        }
                    }
                }
            }
        });
    }
}

-(void)runTest
{
    self.sv1 = [[CCharacterView alloc] initWithSize:CGSizeMake(120.0f,120.0f)];
    //[sv1 setBackgroundColor:[UIColor yellowColor]];
    [self.sv1 setImage:[UIImage imageNamed:@"tailsdoll_mario_8_bit_type_by_thenaruterox100pre-d3748p0.png"]];
    [self.sv1 loadCharacterResourcePackNamed:@"girl"];
    [self.sv1 setOrientation:_CHARACTER_ORIENTATION_LEFT];
    [self addSpriteView:self.sv1 onGridObject:[self getGridObjectHolderAtPosition:4 y:9]];
    self.sv1.tag = 0;
    
    CSpriteView *sv2 = [[CSpriteView alloc] initWithSize:CGSizeMake(120.0f,120.0f)];
    [sv2 setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:1.0f alpha:0.5f]]; // blue square
    [self addSpriteView:sv2 onGridObject:[self getGridObjectHolderAtPosition:4 y:5]];
    sv2.tag = 1;
    
    CGridObjectHolder *newgo = [self getGridObjectHolderAtPosition:2 y:0];
    CGPoint newposition = [newgo getUIPositionForCenteredSprite:self.sv1];
    
    [UIView animateWithDuration:10.0f
                          delay:0.0f
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.sv1.frame = CGRectMake(newposition.x, newposition.y, self.sv1.frame.size.width,self.sv1.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"CPlainViewController touchesBegan");
    
    UITouch *aTouch = [touches anyObject];
    CGPoint pt = [aTouch locationInView:self.touchViewOverlay];
    
    [self onTouchedPosition:pt];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *aTouch = [touches anyObject];
    CGPoint pt = [aTouch locationInView:self.touchViewOverlay];
    
    [self onTouchedPosition:pt];
}

-(float)distanceBetweenPositions:(CGPoint)from new:(CGPoint)to
{
    float xdif = 0.0f;
    float ydif = 0.0f;
    
    if (to.x>from.x) xdif = to.x-from.x;
    else xdif = from.x-to.x;
    
    if (to.y>from.y) ydif = to.y-from.y;
    else ydif = from.y-to.y;
    
    float distance = sqrt((xdif*xdif)+(ydif*ydif));
    
    return distance;
}

-(void)addNextRouteMember:(NSMutableArray*)route current:(CGridObjectHolder*)current destination:(CGridObjectHolder*)to
{
    if (current==to)
        return;
    
    float rightUpDistance = MAXFLOAT;
    float rightDistance = MAXFLOAT;
    float rightDownDistance = MAXFLOAT;
    float downDistance = MAXFLOAT;
    float leftDownDistance = MAXFLOAT;
    float leftDistance = MAXFLOAT;
    float leftUpDistance = MAXFLOAT;
    float upDistance = MAXFLOAT;
    
    float currentLow = MAXFLOAT;
    CGridObjectHolder *nextHolder = nil;
    
    CGridObjectHolder *gh = [self getGridObjectHolderAtPosition:current.go->gx+1 y:current.go->gy-1]; // right-up
    if (gh)
    {
        rightUpDistance = [self distanceBetweenPositions:[gh getSpriteCoordinates] new:[to getSpriteCoordinates]];
        if (currentLow>rightUpDistance)
        {
            currentLow = rightUpDistance;
            nextHolder = gh;
        }
    }
    gh = [self getGridObjectHolderAtPosition:current.go->gx+1 y:current.go->gy]; // right
    if (gh)
    {
        rightDistance = [self distanceBetweenPositions:[gh getSpriteCoordinates] new:[to getSpriteCoordinates]];
        if (currentLow>rightDistance)
        {
            currentLow = rightDistance;
            nextHolder = gh;
        }
    }
    gh = [self getGridObjectHolderAtPosition:current.go->gx+1 y:current.go->gy+1]; // right-down
    if (gh)
    {
        rightDownDistance = [self distanceBetweenPositions:[gh getSpriteCoordinates] new:[to getSpriteCoordinates]];
        if (currentLow>rightDownDistance)
        {
            currentLow = rightDownDistance;
            nextHolder = gh;
        }
    }
    gh = [self getGridObjectHolderAtPosition:current.go->gx y:current.go->gy+1]; // down
    if (gh)
    {
        downDistance = [self distanceBetweenPositions:[gh getSpriteCoordinates] new:[to getSpriteCoordinates]];
        if (currentLow>downDistance)
        {
            currentLow = downDistance;
            nextHolder = gh;
        }
    }
    gh = [self getGridObjectHolderAtPosition:current.go->gx-1 y:current.go->gy+1]; // left-down
    if (gh)
    {
        leftDownDistance = [self distanceBetweenPositions:[gh getSpriteCoordinates] new:[to getSpriteCoordinates]];
        if (currentLow>leftDownDistance)
        {
            currentLow = leftDownDistance;
            nextHolder = gh;
        }
    }
    gh = [self getGridObjectHolderAtPosition:current.go->gx-1 y:current.go->gy]; // left
    if (gh)
    {
        leftDistance = [self distanceBetweenPositions:[gh getSpriteCoordinates] new:[to getSpriteCoordinates]];
        if (currentLow>leftDistance)
        {
            currentLow = leftDistance;
            nextHolder = gh;
        }
    }
    gh = [self getGridObjectHolderAtPosition:current.go->gx-1 y:current.go->gy-1]; // left-up
    if (gh)
    {
        leftUpDistance = [self distanceBetweenPositions:[gh getSpriteCoordinates] new:[to getSpriteCoordinates]];
        if (currentLow>leftUpDistance)
        {
            currentLow = leftUpDistance;
            nextHolder = gh;
        }
    }
    gh = [self getGridObjectHolderAtPosition:current.go->gx y:current.go->gy-1]; // up
    if (gh)
    {
        upDistance = [self distanceBetweenPositions:[gh getSpriteCoordinates] new:[to getSpriteCoordinates]];
        if (currentLow>upDistance)
        {
            currentLow = upDistance;
            nextHolder = gh;
        }
    }
    
    [route addObject:nextHolder];
    [self addNextRouteMember:route current:nextHolder destination:to];
}

-(id)calculateRouteFrom:(CGridObjectHolder*)from to:(CGridObjectHolder*)to
{
    NSLog(@"CPlainViewController calculateRouteFrom to");
    if (!from || !to)
        return nil;
    
    NSMutableArray *route = [[NSMutableArray alloc] init];
    CGridObjectHolder *current = from;
    
    [self addNextRouteMember:route current:current destination:to];
    int n;
    for(n = 0; n < [route count]; n++)
    {
        NSLog(@"r001");
        CGridObjectHolder *tmph = [route objectAtIndex:n];
        NSLog(@"    route (%d,%d)",tmph.go->gx,tmph.go->gy);
    }
    
    [self manifestRoute:route from:from to:to];
    
    return nil;
}

-(void)onRouteObject:(NSMutableDictionary*)routeSetup
{
    NSArray *routeObjects = [routeSetup objectForKey:@"route"];
    NSNumber *currentArrayPosition = [routeSetup objectForKey:@"currentPosition"];
    int position = [currentArrayPosition intValue];
    
    NSLog(@"    animation position(%d)",position);
    
    float duration = 1.0f;
    
    NSLog(@"r002");
    if ([routeObjects count]<=position)
        return;
        
    CGridObjectHolder *gh = [routeObjects objectAtIndex:position];
    if (gh)
    {
        CGPoint currentPosition = [self.sv1 getPosition];
        CGPoint newPosition = [gh getSpriteCoordinates];
        
        float distance = [self distanceBetweenPositions:currentPosition new:newPosition];
        
        duration = distance*0.004f;
        
        // here we calculate the orientation
        
        float screendifx = newPosition.x-currentPosition.x;
        float screendify = newPosition.y-currentPosition.y;
        
        if (screendifx<0.0f)
        {
            // left
            if (screendify==0.0f)
                [self.sv1 setOrientation:_CHARACTER_ORIENTATION_LEFT];
            else
            {
                if (screendify<0.0f)
                {
                    [self.sv1 setOrientation:_CHARACTER_ORIENTATION_UPLEFT];
                }
                else
                {
                    [self.sv1 setOrientation:_CHARACTER_ORIENTATION_DOWNLEFT];
                }
            }
        }
        else if (screendifx>0.0f)
        {
            // right
            if (screendify==0)
                [self.sv1 setOrientation:_CHARACTER_ORIENTATION_RIGHT];
            else
            {
                if (screendify<0.0f)
                {
                    [self.sv1 setOrientation:_CHARACTER_ORIENTATION_UPRIGHT];
                }
                else
                {
                    [self.sv1 setOrientation:_CHARACTER_ORIENTATION_DOWNRIGHT];
                }
            }
        }
        else
        {
            // straight up or down
            if (screendify<0.0f)
            {
                [self.sv1 setOrientation:_CHARACTER_ORIENTATION_UP];
            }
            else if (screendify>0.0f)
            {
                [self.sv1 setOrientation:_CHARACTER_ORIENTATION_DOWN];
            }
        }
        // end orientation calculations
    }
    
    id animationBlock = ^{
        @synchronized(self)
        {
            [self.sv1 setMoving:YES];
            NSArray *routeObjects = [routeSetup objectForKey:@"route"];
            NSNumber *currentArrayPosition = [routeSetup objectForKey:@"currentPosition"];
            int position = [currentArrayPosition intValue];
            
            NSLog(@"    animation position(%d)",position);
            
            NSLog(@"r003");
            if ([routeObjects count]>position)
            {
                
                CGridObjectHolder *gh = [routeObjects objectAtIndex:position];
                if (gh)
                {
                    CGPoint newSpriteposition = [gh getUIPositionForCenteredSprite:self.sv1];
                    
                    self.sv1.frame = CGRectMake(newSpriteposition.x, newSpriteposition.y, self.sv1.frame.size.width,self.sv1.frame.size.height);
                }
                else
                {
                    NSLog(@"    bad");
                }
            }
        }
    };
    
    [UIView animateWithDuration:duration
                          delay:0.0f
                        options: UIViewAnimationOptionCurveLinear
                     animations:animationBlock
                     completion:^(BOOL finished){
                         @synchronized(self)
                         {
                             NSArray *routeObjects = [routeSetup objectForKey:@"route"];
                             NSNumber *currentPosition = [routeSetup objectForKey:@"currentPosition"];
                             int position = [currentPosition intValue];
                             
                             NSLog(@"    completion block(%d)",position);
                             
                             position++;
                             [self.sv1 setMoving:NO];
                             if (position!=[routeObjects count])
                             {
                                 [routeSetup setObject:[NSNumber numberWithInt:position] forKey:@"currentPosition"];
                                 
                                 [self onRouteObject:routeSetup];
                             }
                         }
                     }];
}

-(void)manifestRoute:(NSArray*)route from:(CGridObjectHolder*)from to:(CGridObjectHolder*)to
{
    NSLog(@"CPlainViewController manifestRoute");
    if (!route || !from || !to)
        return;
    @synchronized(self)
    {
        NSMutableDictionary *routeSetup = [[NSMutableDictionary alloc] init];
        [routeSetup setObject:[NSArray arrayWithArray:route] forKey:@"route"]; // mika todo
        [routeSetup setObject:[NSNumber numberWithInt:0] forKey:@"currentPosition"];
        
        [self onRouteObject:routeSetup];
    }
}

-(void)onTouchedPosition:(CGPoint)pt
{
    @synchronized(self)
    {
        static CGridObjectHolder *headingTo = nil;
        
        CGridObjectHolder *gh = [self getGridObjectHolderAtCoordinates:pt];
        if (gh)
        {
            if (gh==headingTo)
                return;
            
            headingTo = gh;
            
            CGRect realTimeFrame = [[self.sv1.layer presentationLayer] frame];
            [self.sv1 setFrame:realTimeFrame];
            [self.sv1.layer removeAllAnimations];
            
            CGPoint currentPosition = [self.sv1 getPosition];
            CGridObjectHolder *currenth = [self getGridObjectHolderAtCoordinates:currentPosition];
            // currenth is the current holder
            
            [self calculateRouteFrom:currenth to:gh];
        }
    }
}

@end
