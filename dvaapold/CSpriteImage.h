//
//  CSpriteImage.h
//  dvaapold
//
//  Created by Milan Kazarka on 1/21/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSpriteImage : NSObject {
@public
}

@property UIImage *image;

-(id)initWithImage:(UIImage*)image;

@end
