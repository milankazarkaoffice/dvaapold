//
//  CSpriteImage.m
//  dvaapold
//
//  Created by Milan Kazarka on 1/21/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import "CSpriteImage.h"

@implementation CSpriteImage

-(id)initWithImage:(UIImage*)image
{
    self = [super init];
    if (self)
    {
        self.image = image;
    }
    return self;
}

@end
