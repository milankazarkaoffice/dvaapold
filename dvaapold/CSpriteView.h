//
//  CSpriteView.h
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSpriteView : UIView

@property UIImageView *imageView;
@property BOOL ignoreZPosition;

-(id)initWithSize:(CGSize)size;
-(CGPoint)getPosition;
-(void)setImage:(UIImage*)image;

@end
