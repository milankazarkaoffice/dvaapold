//
//  CSpriteView.m
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import "CSpriteView.h"

@implementation CSpriteView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

-(id)initWithSize:(CGSize)size
{
    self = [super initWithFrame:CGRectMake(0.0f,0.0f,size.width,size.height)];
    if (self)
    {
        [self setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.5f]];
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:self.imageView];
    }
    return self;
}

-(void)setFrame:(CGRect)frame
{
    NSLog(@"CSpriteView setFrame");
    [super setFrame:frame];
}

-(CGPoint)getPosition
{
    CGPoint pt = CGPointZero;
    
    // if we're in animation, than the current (frame) doesn't give the accurate realTime position
    CGRect realTimeFrame = [[self.layer presentationLayer] frame];
    
    pt.x = realTimeFrame.origin.x+(realTimeFrame.size.width/2.0f);
    pt.y = realTimeFrame.origin.y+(realTimeFrame.size.height/2.0f);
    
    //NSLog(@"CSpriteView getPosition pt(%fx%f)",pt.x,pt.y);
    
    return pt;
}

-(void)setImage:(UIImage*)image
{
    if (!image)
        return;
    [self.imageView setImage:image];
}

@end
