//
//  CTouchEventView.h
//  dvaapold
//
//  Created by Milan Kazarka on 1/10/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CTouchEventViewDelegate <NSObject>
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
@end

@interface CTouchEventView : UIView

@property NSObject<CTouchEventViewDelegate> *delegate;

@end
