//
//  CTouchEventView.m
//  dvaapold
//
//  Created by Milan Kazarka on 1/10/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#import "CTouchEventView.h"

@implementation CTouchEventView

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.delegate)
        [self.delegate touchesBegan:touches withEvent:event];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.delegate)
        [self.delegate touchesMoved:touches withEvent:event];
}

@end
