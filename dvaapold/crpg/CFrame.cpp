//
//  CFrame.cpp
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#include "CFrame.h"

CFrame::CFrame( struct _CFRAME cf )
{
    mframe = cf;
}

struct _CFRAME _CFRAMECREATE( int x, int y, int width, int height )
{
    struct _CFRAME cf;
    cf.origin.x = x;
    cf.origin.y = y;
    cf.size.width = width;
    cf.size.height = height;
    return cf;
}
