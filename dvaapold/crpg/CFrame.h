//
//  CFrame.h
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#ifndef __dvaapold__CFrame__
#define __dvaapold__CFrame__

#include <stdio.h>

struct _CORIGIN {
    float x;
    float y;
};

struct _CSIZE {
    float width;
    float height;
};

struct _CFRAME {
    struct _CORIGIN origin;
    struct _CSIZE size;
};

struct _CFRAME _CFRAMECREATE( int x, int y, int w, int h );

class CFrame {
public:
    struct _CFRAME mframe;
    
    CFrame( _CFRAME );
};

#endif /* defined(__dvaapold__CFrame__) */
