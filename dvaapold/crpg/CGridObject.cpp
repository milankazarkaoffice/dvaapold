//
//  CGridObject.cpp
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#include "CGridObject.h"

CGridObject::CGridObject( struct _CFRAME cf, int x, int y )
: CFrame( cf )
{
    gx = x;
    gy = y;
    tag = 0;
}

CGridObject::~CGridObject( )
{
    
}
