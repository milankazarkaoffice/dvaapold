//
//  CGridObject.h
//  dvaapold
//
//  Created by Milan Kazarka on 1/8/15.
//  Copyright (c) 2015 Milan Kazarka. All rights reserved.
//

#ifndef __dvaapold__CGridObject__
#define __dvaapold__CGridObject__

#include <stdio.h>
#include "CFrame.h"

class CGridObject : public CFrame {
public:
    
    // positioning on the grid (0,0) (1,0) (2,0)
    int gx;
    int gy;
    int tag;
    
    CGridObject( struct _CFRAME cf, int x, int y );
    ~CGridObject( );
};

#endif /* defined(__dvaapold__CGridObject__) */
